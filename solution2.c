#include <stdio.h>

unsigned long read_number();
int is_prime(unsigned long);

int main() {
    unsigned long number = read_number();
    puts(is_prime(number) ? "Yes" : "No");
    return 0;
}

unsigned long read_number() {
    unsigned long result;
    scanf("%lu", &result);
    return result;
}

int is_prime(unsigned long n) {
    if (n <= 1)
        return 0;

    for (int i=2; i<n; i++) 
        if (n%i == 0) 
            return 0;

    return 1;
}