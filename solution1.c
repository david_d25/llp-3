#include <stdio.h>

int array1[] = {2, 7, 4};
int array2[] = {6, 3, 2};
int arraysSize = 3;

int scalarProduct(int*, int*, int);

int main() {
    int product = scalarProduct(array1, array2, arraysSize);
    printf("Result: %d", product);
    return 0;
}

int scalarProduct(int* arrayA, int* arrayB, int size) {
    int result = 0;
    for (int i = 0; i < size; i++)
        result += arrayA[i]*arrayB[i];
    return result;
}